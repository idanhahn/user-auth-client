import { Component, OnInit } from '@angular/core';
import {UserAuthService} from '../services/user-auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {

  userAuth: UserAuthService;
  constructor(userAuth: UserAuthService, private router: Router) {
    this.userAuth = userAuth;
  }

  ngOnInit() {}

  onLogout() {
    console.log('Logout clicked');
    this.userAuth.authenticated = false;
    this.userAuth.deleteToken();
    this.router.navigateByUrl('/login');
  }
}
