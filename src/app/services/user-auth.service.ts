import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {

  constructor(private http: HttpClient) { }

  noAuthHeader = {headers: new HttpHeaders({NoAuth: 'True', 'Access-Control-Allow-Origin': 'True'})};
  authenticated: boolean;


  signup(user) {
    return this.http.post(environment.baseUrl + '/signup', user, this.noAuthHeader);
  }

  login(user) {
    return this.http.post(environment.baseUrl + '/login', user, this.noAuthHeader);
  }

  getUser(username) {
    console.dir(username)
    console.log(environment.baseUrl + `/userProfile/${username}`)
    return this.http.get(environment.baseUrl + `/userProfile/${username}`);
  }

  // Helpers:

  setToken(token) {
    localStorage.setItem('token', token);
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  getUserPayload() {
    const token = this.getToken();
    if (token) {
      return JSON.parse(atob(token.split('.')[1]));
    } else {
      return false;
    }
  }

  isAuthenticated(): boolean {

    const userPayload = this.getUserPayload();
    if (userPayload) {
      return userPayload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

}
