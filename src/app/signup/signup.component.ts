import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserAuthService} from '../services/user-auth.service';
import {ForbiddenPasswordValidator} from '../shared/custom-validators.directive';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;

  errorFromServer = {
    duplicateEmail: false,
    msg: ''
  };

  get email() { return this.signupForm.get('email'); }
  get password() { return this.signupForm.get('password'); }

  constructor(private userAuth: UserAuthService, private router: Router) {
  }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'firstName': new FormControl(null),
      'lastName': new FormControl(null),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [ForbiddenPasswordValidator(), Validators.required])
    })
  }

  onSubmit() {
    console.log(this.signupForm);
    this.userAuth.signup(this.signupForm.value).subscribe( res => {
      console.log('Response from signup');
      console.dir(res);
      // save token and log into secure page
      this.userAuth.setToken(res['token']);
      this.userAuth.authenticated = true;
      this.router.navigateByUrl('/');

    }, err => {
      console.dir(err);
      this.signupForm.get('email').setErrors({duplicateEmail: err.error.errorMsg})
    });
  }

}
