import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserAuthService} from '../services/user-auth.service';
import {Router} from '@angular/router';
import {ForbiddenPasswordValidator} from '../shared/custom-validators.directive';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  constructor(private userAuth: UserAuthService, private router: Router) { }


  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, ForbiddenPasswordValidator()])
    });
  }


  onSubmit() {
    this.userAuth.login(this.loginForm.value).subscribe(res => {
      console.log('Login successfull');
      console.dir(res);
      this.userAuth.setToken(res['token']);
      this.userAuth.authenticated = true;
      this.router.navigateByUrl('/');

    }, error => {
      // check error messages:
      if (error.error['message'] == 'Wrong password') {

        this.loginForm.controls['password'].setErrors({wrongPassword: error.error['message']});

      } else if (error.error['message'] == 'Email doesn\'t exist') {

        this.loginForm.controls['email'].setErrors({noEmail: error.error['message']});

      } else {
        // unknown error
        console.log('unknown login error');
      }

    });


  }

}
