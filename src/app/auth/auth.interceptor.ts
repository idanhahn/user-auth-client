import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {UserAuthService} from '../services/user-auth.service';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private userAuth: UserAuthService, private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    // check if req should be auth
    if (request.headers.get('noAuth')) {
      return next.handle(request);
    }

    // get token
    const token = this.userAuth.getToken();

    // add token to header and send
    return next.handle(request.clone({headers: request.headers.set('authorization', 'Bearer ' + token)})).pipe(
      tap(
        event => {},
        err => {
            console.dir(err);
            // assuming auth error
            this.userAuth.authenticated = false;
            this.router.navigateByUrl('/login');
      })
    );

  }
}
