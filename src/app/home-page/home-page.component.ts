import { Component, OnInit } from '@angular/core';
import {UserAuthService} from '../services/user-auth.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  userDetails;

  constructor(private userAuth: UserAuthService) { }

  ngOnInit() {
    // get user info
    const user = this.userAuth.getUserPayload();
    console.dir(user)
    this.userAuth.getUser(user.username).subscribe( res => {
      console.dir(res);
      this.userDetails = res;
    })
  }

}
